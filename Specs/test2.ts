import { Selector } from 'testcafe';
import {Googlepage} from '../Pages/googlePage';

const gPage = new Googlepage();

fixture `Getting Started`
    .page `https://devexpress.github.io/testcafe/example/`;


  test('Test Scenario #4:Validation of submitbutton enable or disable', async t => {
    await gPage.submitbuttonDisabled();
    await gPage.typeInput('Testing');
    await gPage.selectRadiobutton('MacOS');
    await gPage.selectText('Re-using existing JavaScript code for testing');
    await gPage.submitbuttonEnabled();
    await gPage.clickSubmitButton();
    await t.wait(1000);

  });