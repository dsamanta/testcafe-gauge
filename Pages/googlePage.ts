import {Selector,t} from 'testcafe';

export class Googlepage{

public inputbox:any;
public radioButton:any;
public checkBox:any;
public selectItem:any;
public button:any;

constructor(){
this.inputbox = Selector("[data-testid='name-input']");
//this.radioButton = Selector( "[data-testid='windows-radio']");
this.selectItem =Selector("fieldset").nth(1).find('p').find('label');
this.radioButton = Selector("fieldset").nth(2).find("P").find("label");
this.button = Selector("[data-testid='submit-button']");




}

async typeInput(name:string){
    await t
    .typeText(this.inputbox,name);
}

async selectText(item:string){
    await t
    .click(this.selectItem.withText(item));
}

async selectRadiobutton(item:string){
    await t
    .click(this.radioButton.withText(item));
}


async submitbuttonDisabled(){
    await t.expect(this.button.hasAttribute('disabled')).ok();
}

async submitbuttonEnabled(){
    await t.expect(this.button.hasAttribute('disabled')).notOk();
}

async clickSubmitButton(){
    await t
    .click(this.button);
}

}